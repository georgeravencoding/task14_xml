package com.kolchak.parser.sax;

import com.kolchak.model.Account;
import com.kolchak.model.Bank;
import com.kolchak.model.Deposit;
import com.kolchak.model.Depositor;
import org.xml.sax.SAXException;

import java.util.List;
import java.util.jar.Attributes;

public class SAXDocReader {
    private List<Bank> bankList=null;
    private Bank bank = null;
    private Deposit deposit = null;
    private Depositor depositor = null;
    private Account account = null;

    private boolean bBank_name = false;
    private boolean bLocation = false;
    private boolean bAccount = false;
    private boolean bAccountId = false;
    private boolean bDepositor = false;
    private boolean bDeposit = false;
    private boolean bName = false;
    private boolean bType = false;
    private boolean bAmount = false;
    private boolean bProfitability = false;
    private boolean bTimeConstraints = false;

    public List<Bank> getBankList() {
        return this.bankList;
    }

    public void startElement(String uri, String localName, String qName, Attributes attributes)throws SAXException {
        if (qName.equalsIgnoreCase("bank")) {
            String bankName = attributes.getValue("bank_name");
            bank = new Bank();
            bank.setBank_name(bankName);
        } else if (qName.equalsIgnoreCase("bank_name")) {
            bBank_name = true;
        }
        else if (qName.equalsIgnoreCase("location")) {
            bLocation = true;
        }
        else if (qName.equalsIgnoreCase("account")) {
            bAccount = true;
        }
        else if (qName.equalsIgnoreCase("accountId")) {
            bAccountId = true;
        }
        else if (qName.equalsIgnoreCase("depositor")) {
            bDepositor = true;
        }
        else if (qName.equalsIgnoreCase("deposit")) {
            bDeposit = true;
        }
        else if (qName.equalsIgnoreCase("name")) {
            bName = true;
        }
        else if (qName.equalsIgnoreCase("type")) {
            bType = true;
        }
        else if (qName.equalsIgnoreCase("amount")) {
            bAmount = true;
        }
        else if (qName.equalsIgnoreCase("profitability")) {
            bProfitability = true;
        }
        else if (qName.equalsIgnoreCase("timeConstraints")) {
            bTimeConstraints = true;
        }
    }

    public void  endElement(String uri, String localName, String qName)throws SAXException {
        if (qName.equalsIgnoreCase("bank")) {
            bankList.add(bank);
        }
    }

    public void character(char ch[], int start, int length)throws SAXException {
        if (bName) {
            bank.setBank_name(new String(ch, start, length));
            bName = false;
        }
        /*CONTINUE HERE*/
    }
}
