package com.kolchak.parser.dom;

import com.kolchak.model.Account;
import com.kolchak.model.Bank;
import com.kolchak.model.Deposit;
import com.kolchak.model.Depositor;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.util.ArrayList;
import java.util.List;

public class DOMDocReader {
    public List<Bank> readDoc(Document doc) {
        doc.getDocumentElement().normalize();
        List<Bank> banks = new ArrayList<>();

        NodeList nodeList = doc.getElementsByTagName("bank");
        for (int i = 0; i < nodeList.getLength(); i++) {
            Bank bank = new Bank();
            Node node = nodeList.item(i);
            if (node.getNodeType() == Node.ELEMENT_NODE) {
                Element element = (Element) node;

                bank.setBank_name(element.getElementsByTagName("bank_name").item(0).getTextContent());
                bank.setLocation(element.getElementsByTagName("location").item(0).getTextContent());
                bank.setAccount(getAccount(element.getElementsByTagName("account")));
            }
        }
        return banks;
    }

    private Depositor getDepositor(NodeList nodes) {
        Depositor depositor = new Depositor();
        if (nodes.item(0).getNodeType() == Node.ELEMENT_NODE) {
            Element element = (Element) nodes.item(0);
            depositor.setName(element.getElementsByTagName("name").item(0).getTextContent());
        }
        return depositor;
    }

    private Deposit getDeposit(NodeList nodes) {
        Deposit deposit = new Deposit();
        if (nodes.item(0).getNodeType() == Node.ELEMENT_NODE) {
            Element element = (Element) nodes.item(0);
            deposit.setDepositType(element.getElementsByTagName("type").item(0).getTextContent());
            deposit.setAmount(Integer.parseInt(element.getAttribute("amount")));
            deposit.setProfitability(Float.parseFloat(element.getAttribute("profitability")));
            deposit.setTimeConstraints(Integer.parseInt(element.getAttribute("timeConstraints")));
        }
        return deposit;
    }

    private Account getAccount(NodeList nodes) {
        Account account = new Account();
        if (nodes.item(0).getNodeType() == Node.ELEMENT_NODE) {
            Element element = (Element) nodes.item(0);
            account.setAccountId(Integer.parseInt(element.getAttribute("accountId")));
            account.setDepositor(getDepositor(element.getElementsByTagName("depositor")));
            account.setDeposit(getDeposit(element.getElementsByTagName("deposit")));
        }
        return account;
    }

}
