package com.kolchak.model;

public class Bank {
    private String bank_name;
    private String location;
    private Account account;

    public Bank() {
    }

    public Bank(String bank_name, String location, Account account) {
        this.bank_name = bank_name;
        this.location = location;
        this.account = account;
    }

    public String getBank_name() {
        return bank_name;
    }

    public void setBank_name(String bank_name) {
        this.bank_name = bank_name;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    @Override
    public String toString() {
        return "Bank{" +
                "bank_name='" + bank_name + '\'' +
                ", location='" + location + '\'' +
                ", account=" + account +
                '}';
    }
}
