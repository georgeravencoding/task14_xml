<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:template match="/">
    <html>
        <body>
            <h2>List of accounts</h2>
            <table border="1">
                <tr bgcolor="red">
                    <th>Banks</th>
                    <th>Accounts №</th>
                    <th>Depositor name</th>
                    <th>Deposit type</th>
                    <th>Deposit amount</th>
                    <th>Deposit profitability</th>
                    <th>Deposit timeConstraints</th>
                </tr>
                <xsl:for-each select="banks/bank">
                    <xsl:sort order="descending" select="account/accountId"/>
                    <xsl:if test="account/deposit/amount &gt; 50">
                        <tr>
                            <td><xsl:value-of select="bank_name"/></td>
                            <td><xsl:value-of select="account/accountId"/></td>
                            <td><xsl:value-of select="account/depositor/name"/></td>
                            <td><xsl:value-of select="account/deposit/type"/></td>
                            <td><xsl:value-of select="account/deposit/amount"/></td>
                            <td><xsl:value-of select="account/deposit/profitability"/></td>
                            <td><xsl:value-of select="account/deposit/timeConstraints"/></td>
                        </tr>
                    </xsl:if>
                </xsl:for-each>
            </table>
        </body>
    </html>
</xsl:template>
</xsl:stylesheet>
